/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package libcore.net.http;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.CacheRequest;
import java.net.CacheResponse;
import java.net.HttpURLConnection;
import java.net.ResponseCache;
import java.net.SecureCacheResponse;
import java.net.URI;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.Principal;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;

import libcore.io.Base64;
import libcore.io.IoUtils;
import libcore.io.Streams;

import com.integralblue.httpresponsecache.compat.Charsets;
import com.integralblue.httpresponsecache.compat.MD5;
import com.integralblue.httpresponsecache.compat.Strings;
import com.integralblue.httpresponsecache.compat.java.net.ExtendedResponseCache;
import com.integralblue.httpresponsecache.compat.java.net.ResponseSource;
import com.jakewharton.DiskLruCache;


/**
 * Cache responses in a directory on the file system. Most clients should use
 * {@code android.net.HttpResponseCache}, the stable, documented front end for
 * this.
 */
public final class HttpResponseCache extends ResponseCache implements ExtendedResponseCache {
    // TODO: add APIs to iterate the cache?
    /** The Constant VERSION. */
    private static final int VERSION = 201105;
    
    /** The Constant ENTRY_METADATA. */
    private static final int ENTRY_METADATA = 0;
    
    /** The Constant ENTRY_BODY. */
    private static final int ENTRY_BODY = 1;
    
    /** The Constant ENTRY_COUNT. */
    private static final int ENTRY_COUNT = 2;

    /** The cache. */
    private final DiskLruCache cache;

    /* read and write statistics, all guarded by 'this' */
    /** The write success count. */
    private int writeSuccessCount;
    
    /** The write abort count. */
    private int writeAbortCount;
    
    /** The network count. */
    private int networkCount;
    
    /** The hit count. */
    private int hitCount;
    
    /** The request count. */
    private int requestCount;

    /**
     * Instantiates a new http response cache.
     *
     * @param directory the directory
     * @param maxSize the max size
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public HttpResponseCache(File directory, long maxSize) throws IOException {
        cache = DiskLruCache.open(directory, VERSION, ENTRY_COUNT, maxSize);
    }

    /**
     * Uri to key.
     *
     * @param uri the uri
     * @return the string
     */
    private String uriToKey(URI uri) {
        // try {
        // MessageDigest.getInstance(String) isn't thread safe, but it should be.
        // On Android, if that static method is invoked by multiple threads simultaneously,
        // a ConcurrentModificationException is thrown. This affects only Android -
        // Sun/Oracle/Open JREs all work correctly.
        // see https://code.google.com/p/android/issues/detail?id=37937
        // So use our own MD5 implementation instead.
        MessageDigest messageDigest = new MD5();
        byte[] md5bytes = messageDigest.digest(Strings.getBytes(uri.toString(),Charsets.UTF_8));
        return Strings.bytesToHexString(md5bytes, false);
        // } catch (NoSuchAlgorithmException e) {
        //    throw new AssertionError(e);
        //}
    }

    /**
     * Gets the.
     *
     * @param uri the uri
     * @param requestMethod the request method
     * @param requestHeaders the request headers
     * @return the cache response
     * @see java.net.ResponseCache#get(java.net.URI, java.lang.String, java.util.Map)
     */
    @Override public CacheResponse get(URI uri, String requestMethod,
            Map<String, List<String>> requestHeaders) {
        String key = uriToKey(uri);
        DiskLruCache.Snapshot snapshot;
        Entry entry;
        try {
            snapshot = cache.get(key);
            if (snapshot == null) {
                return null;
            }
            entry = new Entry(new BufferedInputStream(snapshot.getInputStream(ENTRY_METADATA)));
        } catch (IOException e) {
            // Give up because the cache cannot be read.
            return null;
        }

        if (!entry.matches(uri, requestMethod, requestHeaders)) {
            snapshot.close();
            return null;
        }

        return entry.isHttps()
                ? new EntrySecureCacheResponse(entry, snapshot)
                : new EntryCacheResponse(entry, snapshot);
    }

    /**
     * Put.
     *
     * @param uri the uri
     * @param urlConnection the url connection
     * @return the cache request
     * @throws IOException Signals that an I/O exception has occurred.
     * @see java.net.ResponseCache#put(java.net.URI, java.net.URLConnection)
     */
    @Override public CacheRequest put(URI uri, URLConnection urlConnection) throws IOException {
        if (!(urlConnection instanceof HttpURLConnection)) {
            return null;
        }

        HttpURLConnection httpConnection = (HttpURLConnection) urlConnection;
        String requestMethod = httpConnection.getRequestMethod();
        String key = uriToKey(uri);

        if (requestMethod.equals(HttpEngine.POST)
                || requestMethod.equals(HttpEngine.PUT)
                || requestMethod.equals(HttpEngine.DELETE)) {
            try {
                cache.remove(key);
            } catch (IOException ignored) {
                // The cache cannot be written.
            }
            return null;
        } else if (!requestMethod.equals(HttpEngine.GET)) {
            /*
             * Don't cache non-GET responses. We're technically allowed to cache
             * HEAD requests and some POST requests, but the complexity of doing
             * so is high and the benefit is low.
             */
            return null;
        }

        HttpEngine httpEngine = getHttpEngine(httpConnection);
        if (httpEngine == null) {
            // Don't cache unless the HTTP implementation is ours.
            return null;
        }

        ResponseHeaders response = httpEngine.getResponseHeaders();
        if (response.hasVaryAll()) {
            return null;
        }

        RawHeaders varyHeaders = httpEngine.getRequestHeaders().getHeaders().getAll(
                response.getVaryFields());
        Entry entry = new Entry(uri, varyHeaders, httpConnection);
        DiskLruCache.Editor editor = null;
        try {
            editor = cache.edit(key);
            if (editor == null) {
                return null;
            }
            entry.writeTo(editor);
            return new CacheRequestImpl(editor);
        } catch (IOException e) {
            abortQuietly(editor);
            return null;
        }
    }

    /**
     * Handles a conditional request hit by updating the stored cache response
     * with the headers from {@code httpConnection}. The cached response body is
     * not updated. If the stored response has changed since {@code
     * conditionalCacheHit} was returned, this does nothing.
     *
     * @param conditionalCacheHit the conditional cache hit
     * @param httpConnection the http connection
     */
    @SuppressWarnings("resource")
	public void update(CacheResponse conditionalCacheHit, HttpURLConnection httpConnection) {
        HttpEngine httpEngine = getHttpEngine(httpConnection);
        URI uri = httpEngine.getUri();
        ResponseHeaders response = httpEngine.getResponseHeaders();
        RawHeaders varyHeaders = httpEngine.getRequestHeaders().getHeaders()
                .getAll(response.getVaryFields());
        Entry entry = new Entry(uri, varyHeaders, httpConnection);
        DiskLruCache.Snapshot snapshot = (conditionalCacheHit instanceof EntryCacheResponse)
                ? ((EntryCacheResponse) conditionalCacheHit).snapshot
                : ((EntrySecureCacheResponse) conditionalCacheHit).snapshot;
        DiskLruCache.Editor editor = null;
        try {
            editor = snapshot.edit(); // returns null if snapshot is not current
            if (editor != null) {
                entry.writeTo(editor);
                editor.commit();
            }
        } catch (IOException e) {
            abortQuietly(editor);
        }
    }

    /**
     * Abort quietly.
     *
     * @param editor the editor
     */
    private void abortQuietly(DiskLruCache.Editor editor) {
        // Give up because the cache cannot be written.
        try {
            if (editor != null) {
                editor.abort();
            }
        } catch (IOException ignored) {
        }
    }

    /**
     * Gets the http engine.
     *
     * @param httpConnection the http connection
     * @return the http engine
     */
    private HttpEngine getHttpEngine(HttpURLConnection httpConnection) {
        if (httpConnection instanceof HttpURLConnectionImpl) {
            return ((HttpURLConnectionImpl) httpConnection).getHttpEngine();
        } else if (httpConnection instanceof HttpsURLConnectionImpl) {
            return ((HttpsURLConnectionImpl) httpConnection).getHttpEngine();
        } else {
            return null;
        }
    }

    /**
     * Gets the cache.
     *
     * @return the cache
     */
    public DiskLruCache getCache() {
        return cache;
    }

    /**
     * Gets the write abort count.
     *
     * @return the write abort count
     */
    public synchronized int getWriteAbortCount() {
        return writeAbortCount;
    }

    /**
     * Gets the write success count.
     *
     * @return the write success count
     */
    public synchronized int getWriteSuccessCount() {
        return writeSuccessCount;
    }

    /**
     * Track response.
     *
     * @param source the source
     * @see com.integralblue.httpresponsecache.compat.java.net.ExtendedResponseCache#trackResponse(com.integralblue.httpresponsecache.compat.java.net.ResponseSource)
     */
    public synchronized void trackResponse(ResponseSource source) {
        requestCount++;

        switch (source) {
        case CACHE:
            hitCount++;
            break;
        case CONDITIONAL_CACHE:
        case NETWORK:
            networkCount++;
            break;
        }
    }

    /**
     * Track conditional cache hit.
     *
     * @see com.integralblue.httpresponsecache.compat.java.net.ExtendedResponseCache#trackConditionalCacheHit()
     */
    public synchronized void trackConditionalCacheHit() {
        hitCount++;
    }

    /**
     * Gets the network count.
     *
     * @return the network count
     */
    public synchronized int getNetworkCount() {
        return networkCount;
    }

    /**
     * Gets the hit count.
     *
     * @return the hit count
     */
    public synchronized int getHitCount() {
        return hitCount;
    }

    /**
     * Gets the request count.
     *
     * @return the request count
     */
    public synchronized int getRequestCount() {
        return requestCount;
    }

    /**
     * The Class CacheRequestImpl.
     */
    private final class CacheRequestImpl extends CacheRequest {
        
        /** The editor. */
        private final DiskLruCache.Editor editor;
        
        /** The cache out. */
        private OutputStream cacheOut;
        
        /** The done. */
        private boolean done;
        
        /** The body. */
        private OutputStream body;

        /**
         * Instantiates a new cache request impl.
         *
         * @param editor the editor
         * @throws IOException Signals that an I/O exception has occurred.
         */
        public CacheRequestImpl(final DiskLruCache.Editor editor) throws IOException {
            this.editor = editor;
            this.cacheOut = editor.newOutputStream(ENTRY_BODY);
            this.body = new FilterOutputStream(cacheOut) {
                @Override public void close() throws IOException {
                    synchronized (HttpResponseCache.this) {
                        if (done) {
                            return;
                        }
                        done = true;
                        writeSuccessCount++;
                    }
                    super.close();
                    editor.commit();
                }

                @Override
                public void write(byte[] buffer, int offset, int length) throws IOException {
                    // Since we don't override "write(int oneByte)", we can write directly to "out"
                    // and avoid the inefficient implementation from the FilterOutputStream.
                    out.write(buffer, offset, length);
                }
            };
        }

        /**
         * Abort.
         *
         * @see java.net.CacheRequest#abort()
         */
        @Override public void abort() {
            synchronized (HttpResponseCache.this) {
                if (done) {
                    return;
                }
                done = true;
                writeAbortCount++;
            }
            IoUtils.closeQuietly(cacheOut);
            try {
                editor.abort();
            } catch (IOException ignored) {
            }
        }

        /**
         * Gets the body.
         *
         * @return the body
         * @throws IOException Signals that an I/O exception has occurred.
         * @see java.net.CacheRequest#getBody()
         */
        @Override public OutputStream getBody() throws IOException {
            return body;
        }
    }

    /**
     * The Class Entry.
     */
    private static final class Entry {
        
        /** The uri. */
        private final String uri;
        
        /** The vary headers. */
        private final RawHeaders varyHeaders;
        
        /** The request method. */
        private final String requestMethod;
        
        /** The response headers. */
        private final RawHeaders responseHeaders;
        
        /** The cipher suite. */
        private final String cipherSuite;
        
        /** The peer certificates. */
        private final Certificate[] peerCertificates;
        
        /** The local certificates. */
        private final Certificate[] localCertificates;

        /*
         * Reads an entry from an input stream. A typical entry looks like this:
         *   http://google.com/foo
         *   GET
         *   2
         *   Accept-Language: fr-CA
         *   Accept-Charset: UTF-8
         *   HTTP/1.1 200 OK
         *   3
         *   Content-Type: image/png
         *   Content-Length: 100
         *   Cache-Control: max-age=600
         *
         * A typical HTTPS file looks like this:
         *   https://google.com/foo
         *   GET
         *   2
         *   Accept-Language: fr-CA
         *   Accept-Charset: UTF-8
         *   HTTP/1.1 200 OK
         *   3
         *   Content-Type: image/png
         *   Content-Length: 100
         *   Cache-Control: max-age=600
         *
         *   AES_256_WITH_MD5
         *   2
         *   base64-encoded peerCertificate[0]
         *   base64-encoded peerCertificate[1]
         *   -1
         *
         * The file is newline separated. The first two lines are the URL and
         * the request method. Next is the number of HTTP Vary request header
         * lines, followed by those lines.
         *
         * Next is the response status line, followed by the number of HTTP
         * response header lines, followed by those lines.
         *
         * HTTPS responses also contain SSL session information. This begins
         * with a blank line, and then a line containing the cipher suite. Next
         * is the length of the peer certificate chain. These certificates are
         * base64-encoded and appear each on their own line. The next line
         * contains the length of the local certificate chain. These
         * certificates are also base64-encoded and appear each on their own
         * line. A length of -1 is used to encode a null array.
         */
        /**
         * Instantiates a new entry.
         *
         * @param in the in
         * @throws IOException Signals that an I/O exception has occurred.
         */
        public Entry(InputStream in) throws IOException {
            try {
                uri = Streams.readAsciiLine(in);
                requestMethod = Streams.readAsciiLine(in);
                varyHeaders = new RawHeaders();
                int varyRequestHeaderLineCount = readInt(in);
                for (int i = 0; i < varyRequestHeaderLineCount; i++) {
                    varyHeaders.addLine(Streams.readAsciiLine(in));
                }

                responseHeaders = new RawHeaders();
                responseHeaders.setStatusLine(Streams.readAsciiLine(in));
                int responseHeaderLineCount = readInt(in);
                for (int i = 0; i < responseHeaderLineCount; i++) {
                    responseHeaders.addLine(Streams.readAsciiLine(in));
                }

                if (isHttps()) {
                    String blank = Streams.readAsciiLine(in);
                    if (!Strings.isEmpty(blank)) {
                        throw new IOException("expected \"\" but was \"" + blank + "\"");
                    }
                    cipherSuite = Streams.readAsciiLine(in);
                    peerCertificates = readCertArray(in);
                    localCertificates = readCertArray(in);
                } else {
                    cipherSuite = null;
                    peerCertificates = null;
                    localCertificates = null;
                }
            } finally {
                in.close();
            }
        }

        /**
         * Instantiates a new entry.
         *
         * @param uri the uri
         * @param varyHeaders the vary headers
         * @param httpConnection the http connection
         */
        public Entry(URI uri, RawHeaders varyHeaders, HttpURLConnection httpConnection) {
            this.uri = uri.toString();
            this.varyHeaders = varyHeaders;
            this.requestMethod = httpConnection.getRequestMethod();
            this.responseHeaders = RawHeaders.fromMultimap(httpConnection.getHeaderFields());

            if (isHttps()) {
                HttpsURLConnection httpsConnection = (HttpsURLConnection) httpConnection;
                cipherSuite = httpsConnection.getCipherSuite();
                Certificate[] peerCertificatesNonFinal = null;
                try {
                    peerCertificatesNonFinal = httpsConnection.getServerCertificates();
                } catch (SSLPeerUnverifiedException ignored) {
                }
                peerCertificates = peerCertificatesNonFinal;
                localCertificates = httpsConnection.getLocalCertificates();
            } else {
                cipherSuite = null;
                peerCertificates = null;
                localCertificates = null;
            }
        }

        /**
         * Write to.
         *
         * @param editor the editor
         * @throws IOException Signals that an I/O exception has occurred.
         */
        public void writeTo(DiskLruCache.Editor editor) throws IOException {
            OutputStream out = editor.newOutputStream(ENTRY_METADATA);
            Writer writer = new BufferedWriter(new OutputStreamWriter(out, Charsets.UTF_8));

            writer.write(uri + '\n');
            writer.write(requestMethod + '\n');
            writer.write(Integer.toString(varyHeaders.length()) + '\n');
            for (int i = 0; i < varyHeaders.length(); i++) {
                writer.write(varyHeaders.getFieldName(i) + ": "
                        + varyHeaders.getValue(i) + '\n');
            }

            writer.write(responseHeaders.getStatusLine() + '\n');
            writer.write(Integer.toString(responseHeaders.length()) + '\n');
            for (int i = 0; i < responseHeaders.length(); i++) {
                writer.write(responseHeaders.getFieldName(i) + ": "
                        + responseHeaders.getValue(i) + '\n');
            }

            if (isHttps()) {
                writer.write('\n');
                writer.write(cipherSuite + '\n');
                writeCertArray(writer, peerCertificates);
                writeCertArray(writer, localCertificates);
            }
            writer.close();
        }

        /**
         * Checks if is https.
         *
         * @return true, if is https
         */
        private boolean isHttps() {
            return uri.startsWith("https://");
        }

        /**
         * Read int.
         *
         * @param in the in
         * @return the int
         * @throws IOException Signals that an I/O exception has occurred.
         */
        private int readInt(InputStream in) throws IOException {
            String intString = Streams.readAsciiLine(in);
            try {
                return Integer.parseInt(intString);
            } catch (NumberFormatException e) {
                throw new IOException("expected an int but was \"" + intString + "\"");
            }
        }

        /**
         * Read cert array.
         *
         * @param in the in
         * @return the certificate[]
         * @throws IOException Signals that an I/O exception has occurred.
         */
        private Certificate[] readCertArray(InputStream in) throws IOException {
            int length = readInt(in);
            if (length == -1) {
                return null;
            }
            try {
                CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
                Certificate[] result = new Certificate[length];
                for (int i = 0; i < result.length; i++) {
                    String line = Streams.readAsciiLine(in);
                    byte[] bytes = Base64.decode(Strings.getBytes(line,Charsets.US_ASCII));
                    result[i] = certificateFactory.generateCertificate(
                            new ByteArrayInputStream(bytes));
                }
                return result;
            } catch (CertificateException e) {
                throw new IOException(e.toString());
            }
        }

        /**
         * Write cert array.
         *
         * @param writer the writer
         * @param certificates the certificates
         * @throws IOException Signals that an I/O exception has occurred.
         */
        private void writeCertArray(Writer writer, Certificate[] certificates) throws IOException {
            if (certificates == null) {
                writer.write("-1\n");
                return;
            }
            try {
                writer.write(Integer.toString(certificates.length) + '\n');
                for (Certificate certificate : certificates) {
                    byte[] bytes = certificate.getEncoded();
                    String line = Base64.encode(bytes);
                    writer.write(line + '\n');
                }
            } catch (CertificateEncodingException e) {
                throw new IOException(e.toString());
            }
        }

        /**
         * Matches.
         *
         * @param uri the uri
         * @param requestMethod the request method
         * @param requestHeaders the request headers
         * @return true, if successful
         */
        public boolean matches(URI uri, String requestMethod,
                Map<String, List<String>> requestHeaders) {
            return this.uri.equals(uri.toString())
                    && this.requestMethod.equals(requestMethod)
                    && new ResponseHeaders(uri, responseHeaders)
                            .varyMatches(varyHeaders.toMultimap(), requestHeaders);
        }
    }

    /**
     * Returns an input stream that reads the body of a snapshot, closing the
     * snapshot when the stream is closed.
     *
     * @param snapshot the snapshot
     * @return the input stream
     */
    private static InputStream newBodyInputStream(final DiskLruCache.Snapshot snapshot) {
        return new FilterInputStream(snapshot.getInputStream(ENTRY_BODY)) {
            @Override public void close() throws IOException {
                snapshot.close();
                super.close();
            }
        };
    }

    /**
     * The Class EntryCacheResponse.
     */
    static class EntryCacheResponse extends CacheResponse {
        
        /** The entry. */
        private final Entry entry;
        
        /** The snapshot. */
        private final DiskLruCache.Snapshot snapshot;
        
        /** The in. */
        private final InputStream in;

        /**
         * Instantiates a new entry cache response.
         *
         * @param entry the entry
         * @param snapshot the snapshot
         */
        public EntryCacheResponse(Entry entry, DiskLruCache.Snapshot snapshot) {
            this.entry = entry;
            this.snapshot = snapshot;
            this.in = newBodyInputStream(snapshot);
        }

        /**
         * Gets the headers.
         *
         * @return the headers
         * @see java.net.CacheResponse#getHeaders()
         */
        @Override public Map<String, List<String>> getHeaders() {
            return entry.responseHeaders.toMultimap();
        }

        /**
         * Gets the body.
         *
         * @return the body
         * @see java.net.CacheResponse#getBody()
         */
        @Override public InputStream getBody() {
            return in;
        }
    }

    /**
     * The Class EntrySecureCacheResponse.
     */
    static class EntrySecureCacheResponse extends SecureCacheResponse {
        
        /** The entry. */
        private final Entry entry;
        
        /** The snapshot. */
        private final DiskLruCache.Snapshot snapshot;
        
        /** The in. */
        private final InputStream in;

        /**
         * Instantiates a new entry secure cache response.
         *
         * @param entry the entry
         * @param snapshot the snapshot
         */
        public EntrySecureCacheResponse(Entry entry, DiskLruCache.Snapshot snapshot) {
            this.entry = entry;
            this.snapshot = snapshot;
            this.in = newBodyInputStream(snapshot);
        }

        /**
         * Gets the headers.
         *
         * @return the headers
         * @see java.net.CacheResponse#getHeaders()
         */
        @Override public Map<String, List<String>> getHeaders() {
            return entry.responseHeaders.toMultimap();
        }

        /**
         * Gets the body.
         *
         * @return the body
         * @see java.net.CacheResponse#getBody()
         */
        @Override public InputStream getBody() {
            return in;
        }

        /**
         * Gets the cipher suite.
         *
         * @return the cipher suite
         * @see java.net.SecureCacheResponse#getCipherSuite()
         */
        @Override public String getCipherSuite() {
            return entry.cipherSuite;
        }

        /**
         * Gets the server certificate chain.
         *
         * @return the server certificate chain
         * @throws SSLPeerUnverifiedException the sSL peer unverified exception
         * @see java.net.SecureCacheResponse#getServerCertificateChain()
         */
        @Override public List<Certificate> getServerCertificateChain()
                throws SSLPeerUnverifiedException {
            if (entry.peerCertificates == null || entry.peerCertificates.length == 0) {
                throw new SSLPeerUnverifiedException(null);
            }
            return Arrays.asList(entry.peerCertificates.clone());
        }

        /**
         * Gets the peer principal.
         *
         * @return the peer principal
         * @throws SSLPeerUnverifiedException the sSL peer unverified exception
         * @see java.net.SecureCacheResponse#getPeerPrincipal()
         */
        @Override public Principal getPeerPrincipal() throws SSLPeerUnverifiedException {
            if (entry.peerCertificates == null || entry.peerCertificates.length == 0) {
                throw new SSLPeerUnverifiedException(null);
            }
            return ((X509Certificate) entry.peerCertificates[0]).getSubjectX500Principal();
        }

        /**
         * Gets the local certificate chain.
         *
         * @return the local certificate chain
         * @see java.net.SecureCacheResponse#getLocalCertificateChain()
         */
        @Override public List<Certificate> getLocalCertificateChain() {
            if (entry.localCertificates == null || entry.localCertificates.length == 0) {
                return null;
            }
            return Arrays.asList(entry.localCertificates.clone());
        }

        /**
         * Gets the local principal.
         *
         * @return the local principal
         * @see java.net.SecureCacheResponse#getLocalPrincipal()
         */
        @Override public Principal getLocalPrincipal() {
            if (entry.localCertificates == null || entry.localCertificates.length == 0) {
                return null;
            }
            return ((X509Certificate) entry.localCertificates[0]).getSubjectX500Principal();
        }
    }
}
