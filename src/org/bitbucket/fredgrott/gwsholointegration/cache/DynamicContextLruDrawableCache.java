package org.bitbucket.fredgrott.gwsholointegration.cache;

import android.graphics.drawable.Drawable;
import android.util.LruCache;


/**
 * pre jwilson's suggestion.
 *
 * @author fredgrott
 */
public class DynamicContextLruDrawableCache {
	
	/** The cache. */
	LruCache<ContextAndKey<String>, Drawable> cache;
	
	/**
	 * Instantiates a new dynamic context lru drawable cache.
	 *
	 * @param maxSize the max size
	 */
	public DynamicContextLruDrawableCache(int maxSize) {
		cache = new LruCache<ContextAndKey<String>, Drawable>(maxSize);
	}
	
	/**
	 * Gets the drawable.
	 *
	 * @param string the string
	 * @return the drawable
	 */
	public Drawable getDrawable(ContextAndKey<String> string){
		return cache.get(string);
	}
	
	/**
	 * Put drawable.
	 *
	 * @param string the string
	 * @param draw the draw
	 */
	public void putDrawable(ContextAndKey<String> string, Drawable draw){
		cache.put(string, draw);
		
	}
	
	/**
	 * Clean.
	 */
	public void clean(){
		cache.evictAll();
	}
}