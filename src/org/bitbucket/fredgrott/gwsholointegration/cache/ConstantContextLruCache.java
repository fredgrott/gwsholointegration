package org.bitbucket.fredgrott.gwsholointegration.cache;

import android.content.Context;
import android.util.LruCache;

/**
 * Per jwilson's suggest.
 *
 * @param <K> the key type
 * @param <V> the value type
 * @author fredgrott
 */
@SuppressWarnings("rawtypes")
public class ConstantContextLruCache<K, V> extends LruCache {

	 /** The context. */
 	@SuppressWarnings("unused")
	private final Context context;
	
	/**
	 * Instantiates a new constant context lru cache.
	 *
	 * @param maxSize the max size
	 * @param context the context
	 */
	public ConstantContextLruCache(int maxSize, Context context) {
		super(maxSize);
		this.context = context;
	}

}