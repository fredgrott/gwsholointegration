package org.bitbucket.fredgrott.gwsholointegration.os;

import android.os.StrictMode;


/**
 * StrictModeWrapper is a wrapper class for the android class android.os.StrictMode provided with Android 2.3 onwards.
 * It allows usage of StrictMode class on devices/emulators with Android 2.3 or higher, while providing an availability
 * check so that the code can stay in-situ for lower platform versions. See the application class for usage.
 *
 * @author Manfred Moser <manfred@simpligility.com>
 *
 * @see "http://android-developers.blogspot.com/2009/04/backward-compatibility-for-android.html"
 * @see "http://android-developers.blogspot.com/2010/12/new-gingerbread-api-strictmode.html"
 * 
 * 
 * Modification by @author fredgrott, Apache License 2.0.
 */
public class StrictModeWrapper {
  
	/* class initialization fails when this throws an exception */
	   static {
	       try {
	           Class.forName("android.os.StrictMode", true, Thread.currentThread().getContextClassLoader());
	       } catch (Exception ex) {
	           throw new RuntimeException(ex);
	       }
	   }

	    /**
	     * Check if the class android.os.StrictMode is available at runtime.
	     */
	   public static void checkAvailable() {}


	    /**
	     * Call StrictMode.enableDefaults().
	     */
	    public static void enableDefaults() {
	       StrictMode.enableDefaults();
	    }

	    // all the implementation below is not tested but it should work ... feel free to check and send me fixes..
	    /**
    	 * Sets the thread policy.
    	 *
    	 * @param policy the new thread policy
    	 */
    	public static  void setThreadPolicy(android.os.StrictMode.ThreadPolicy policy) {
	        StrictMode.setThreadPolicy(policy);
	    }

	    /**
    	 * Gets the thread policy.
    	 *
    	 * @return the thread policy
    	 */
    	public static  android.os.StrictMode.ThreadPolicy getThreadPolicy() {
	        return StrictMode.getThreadPolicy();
	    }

	    /**
    	 * Allow thread disk writes.
    	 *
    	 * @return the android.os. strict mode. thread policy
    	 */
    	public static  android.os.StrictMode.ThreadPolicy allowThreadDiskWrites() {
	        return StrictMode.allowThreadDiskWrites();
	    }

	    /**
    	 * Allow thread disk reads.
    	 *
    	 * @return the android.os. strict mode. thread policy
    	 */
    	public static  android.os.StrictMode.ThreadPolicy allowThreadDiskReads() {
	        return StrictMode.allowThreadDiskReads();
	    }

	    /**
    	 * Sets the vm policy.
    	 *
    	 * @param policy the new vm policy
    	 */
    	public static  void setVmPolicy(android.os.StrictMode.VmPolicy policy) {
	        StrictMode.setVmPolicy(policy);
	    }

	    /**
    	 * Gets the vm policy.
    	 *
    	 * @return the vm policy
    	 */
    	public static android.os.StrictMode.VmPolicy getVmPolicy() {
	        return StrictMode.getVmPolicy();
	    }

	    /**
    	 * The Class ThreadPolicyWrapper.
    	 */
    	public static final class ThreadPolicyWrapper
	    {
	        
        	/**
        	 * The Class BuilderWrapper.
        	 */
        	public static final class BuilderWrapper
	        {
	            
            	/** The builder instance. */
            	StrictMode.ThreadPolicy.Builder builderInstance;

	            /**
            	 * Instantiates a new builder wrapper.
            	 */
            	public  BuilderWrapper() {
	                    builderInstance = new StrictMode.ThreadPolicy.Builder();
	            }

	            /**
            	 * Instantiates a new builder wrapper.
            	 *
            	 * @param policy the policy
            	 */
            	public  BuilderWrapper(android.os.StrictMode.ThreadPolicy policy) {
	                    builderInstance = new StrictMode.ThreadPolicy.Builder(policy);
	            }

	            /**
            	 * Detect all.
            	 *
            	 * @return the android.os. strict mode. thread policy. builder
            	 */
            	public android.os.StrictMode.ThreadPolicy.Builder detectAll() {
	                return builderInstance.detectAll();
	            }

	            /**
            	 * Permit all.
            	 *
            	 * @return the android.os. strict mode. thread policy. builder
            	 */
            	public android.os.StrictMode.ThreadPolicy.Builder permitAll() {
	                return builderInstance.permitAll();
	            }

	            /**
            	 * Detect network.
            	 *
            	 * @return the android.os. strict mode. thread policy. builder
            	 */
            	public android.os.StrictMode.ThreadPolicy.Builder detectNetwork() {
	                return builderInstance.detectNetwork();
	            }

	            /**
            	 * Permit network.
            	 *
            	 * @return the android.os. strict mode. thread policy. builder
            	 */
            	public android.os.StrictMode.ThreadPolicy.Builder permitNetwork() {
	                return builderInstance.permitNetwork();
	            }

	            /**
            	 * Detect disk reads.
            	 *
            	 * @return the android.os. strict mode. thread policy. builder
            	 */
            	public android.os.StrictMode.ThreadPolicy.Builder detectDiskReads() {
	                return builderInstance.detectDiskReads();
	            }

	            /**
            	 * Permit disk reads.
            	 *
            	 * @return the android.os. strict mode. thread policy. builder
            	 */
            	public android.os.StrictMode.ThreadPolicy.Builder permitDiskReads() {
	                return builderInstance.permitDiskReads();
	            }

	            /**
            	 * Detect disk writes.
            	 *
            	 * @return the android.os. strict mode. thread policy. builder
            	 */
            	public android.os.StrictMode.ThreadPolicy.Builder detectDiskWrites() {
	                return builderInstance.detectDiskWrites();
	            }

	            /**
            	 * Permit disk writes.
            	 *
            	 * @return the android.os. strict mode. thread policy. builder
            	 */
            	public android.os.StrictMode.ThreadPolicy.Builder permitDiskWrites() {
	                return builderInstance.permitDiskWrites();
	            }

	            /**
            	 * Penalty dialog.
            	 *
            	 * @return the android.os. strict mode. thread policy. builder
            	 */
            	public android.os.StrictMode.ThreadPolicy.Builder penaltyDialog() {
	                return builderInstance.penaltyDialog();
	            }

	            /**
            	 * Penalty death.
            	 *
            	 * @return the android.os. strict mode. thread policy. builder
            	 */
            	public android.os.StrictMode.ThreadPolicy.Builder penaltyDeath() {
	                return builderInstance.penaltyDeath();
	            }

	            /**
            	 * Penalty log.
            	 *
            	 * @return the android.os. strict mode. thread policy. builder
            	 */
            	public android.os.StrictMode.ThreadPolicy.Builder penaltyLog() {
	                return builderInstance.penaltyLog();
	            }

	            /**
            	 * Penalty drop box.
            	 *
            	 * @return the android.os. strict mode. thread policy. builder
            	 */
            	public android.os.StrictMode.ThreadPolicy.Builder penaltyDropBox() {
	                return builderInstance.penaltyDropBox();
	            }

	            /**
            	 * Builds the.
            	 *
            	 * @return the android.os. strict mode. thread policy
            	 */
            	public android.os.StrictMode.ThreadPolicy build() {
	                return builderInstance.build();
	            }
	        }
	    }

	    /**
    	 * The Class VmPolicyWrapper.
    	 */
    	public static final class VmPolicyWrapper {
	        
        	/**
        	 * The Class BuilderWrapper.
        	 */
        	public static final class BuilderWrapper {
	            
            	/** The builder instance. */
            	private StrictMode.VmPolicy.Builder builderInstance;

	            /**
            	 * Instantiates a new builder wrapper.
            	 */
            	public BuilderWrapper() {
	                builderInstance = new StrictMode.VmPolicy.Builder();
	            }

	            /**
            	 * Detect all.
            	 *
            	 * @return the android.os. strict mode. vm policy. builder
            	 */
            	public android.os.StrictMode.VmPolicy.Builder detectAll() {
	                return builderInstance.detectAll();
	            }

	            /**
            	 * Detect leaked sql lite objects.
            	 *
            	 * @return the android.os. strict mode. vm policy. builder
            	 */
            	public android.os.StrictMode.VmPolicy.Builder detectLeakedSqlLiteObjects() {
	                return builderInstance.detectLeakedSqlLiteObjects();
	            }

	            /**
            	 * Penalty death.
            	 *
            	 * @return the android.os. strict mode. vm policy. builder
            	 */
            	public android.os.StrictMode.VmPolicy.Builder penaltyDeath() {
	                return builderInstance.penaltyDeath();
	            }

	            /**
            	 * Penalty log.
            	 *
            	 * @return the android.os. strict mode. vm policy. builder
            	 */
            	public android.os.StrictMode.VmPolicy.Builder penaltyLog() {
	                return builderInstance.penaltyLog();
	            }

	            /**
            	 * Penalty drop box.
            	 *
            	 * @return the android.os. strict mode. vm policy. builder
            	 */
            	public android.os.StrictMode.VmPolicy.Builder penaltyDropBox() {
	                return builderInstance.penaltyDropBox();
	            }

	            /**
            	 * Builds the.
            	 *
            	 * @return the android.os. strict mode. vm policy
            	 */
            	public android.os.StrictMode.VmPolicy build() {
	                return builderInstance.build();
	            }
	        }
	    }
	    // add more wrapping as desired..
}
