package com.integralblue.httpresponsecache.compat.java.net;


/**
 * The Class SocketException.
 */
@SuppressWarnings("serial")
public class SocketException extends java.net.SocketException {

    /**
     * Instantiates a new socket exception.
     */
    public SocketException() {
        super();
    }

    /**
     * Instantiates a new socket exception.
     *
     * @param detailMessage the detail message
     */
    public SocketException(String detailMessage) {
        super(detailMessage);
    }

    /**
     * Instantiates a new socket exception.
     *
     * @param detailMessage the detail message
     * @param cause the cause
     */
    public SocketException(String detailMessage, Throwable cause) {
        super(detailMessage + "\n" + cause.toString());
    }
}
