package com.jakewharton;

import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;


/** 
 * From libcore.io.Streams, from Android 4.1
 */
class Streams {
    
    /**
     * Read fully.
     *
     * @param reader the reader
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    static String readFully(Reader reader) throws IOException {
        try {
            StringWriter writer = new StringWriter();
            char[] buffer = new char[1024];
            int count;
            while ((count = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, count);
            }
            return writer.toString();
        } finally {
            reader.close();
        }
    }
}
