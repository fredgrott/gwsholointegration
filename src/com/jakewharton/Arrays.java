package com.jakewharton;

import java.lang.reflect.Array;



/**
 * 
 * The Class Arrays, from the java.util.Arrays class of Android 4.1
 */
class Arrays {
    
    /**
     * Copy of range.
     *
     * @param <T> the generic type
     * @param original the original
     * @param start the start
     * @param end the end
     * @return the t[]
     */
    @SuppressWarnings("unchecked")
    static <T> T[] copyOfRange(T[] original, int start, int end) {
        int originalLength = original.length; // For exception priority compatibility.
        if (start > end) {
            throw new IllegalArgumentException();
        }
        if (start < 0 || start > originalLength) {
            throw new ArrayIndexOutOfBoundsException();
        }
        int resultLength = end - start;
        int copyLength = Math.min(resultLength, originalLength - start);
        T[] result = (T[]) Array.newInstance(original.getClass().getComponentType(), resultLength);
        System.arraycopy(original, start, result, 0, copyLength);
        return result;
    }
}
